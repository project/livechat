# LiveChat

## Introduction

LiveChat is a premium live chat and help desk software for customer service and sales. It helps online businesses grow sales by inviting visitors to chat before they leave the website. LiveChat comes with a built-in help desk for managing all support activities from one place.

## Installation

Installing LiveChat in Drupal:
1. Upload `livechat` directory to `/sites/all/modules/`.
2. Open your Drupal website.
3. Go to "Administration > Extend" and activate `LiveChat`
module (under `Other` category).
4. Go to "Administration > Configuration > Services > LiveChat"
and follow the instructions.

## References

For further information, please visit the Livechat website:
www.livechatinc.com

## Maintainers

- Mark Casias (markie) - https://www.drupal.org/u/markie
- Michael Wojcik (michael_wojcik) - https://www.drupal.org/u/michael_wojcik
- Austin Wheeler (TimelessDomain) - https://www.drupal.org/u/timelessdomain
- Joakim Stai (ximo) - https://www.drupal.org/u/ximo

